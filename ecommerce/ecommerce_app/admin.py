from django.contrib import admin
from .models import Product, Category, Subcategory, AddCategory_Counter, AddProduct_Counter
# Register your models here.

admin.site.register(Product)
admin.site.register(Category)
admin.site.register(Subcategory)
admin.site.register(AddCategory_Counter)
admin.site.register(AddProduct_Counter)


