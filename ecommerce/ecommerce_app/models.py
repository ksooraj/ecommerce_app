from django.db import models
from django.utils import timezone
# Create your models here.
class Product(models.Model):
	name = models.CharField(max_length= 200)
	price = models.IntegerField()
	quantity = models.IntegerField()
	category_name = models.ForeignKey('Category', on_delete= models.CASCADE, null= True, default= None)
	# created_product =models.DateField(auto_now_add=True)
	# subcategory = models.ForeignKey('Subcategory', on_delete=models.CASCADE)


class Category(models.Model):
	# category_id = models.AutoField(primary_key=True)
	category_name = models.CharField(max_length = 200, primary_key= True)


class Subcategory(models.Model):
	# subcategory_id = models.AutoField(primary_key=True)
	category_name = models.ForeignKey('Category', on_delete=models.CASCADE, null= True, default= None)
	name = models.CharField(max_length = 200)
	# created_subcatogory = models.DateField(auto_now_add=True)


class AddCategory_Counter(models.Model):
	user_name = models.CharField(max_length= 200)
	date_value = models.DateField(default= timezone.now().date())
	count = models.IntegerField(default= 0)

class AddProduct_Counter(models.Model):
	user_name = models.CharField(max_length= 200)
	date_value = models.DateField(default= timezone.now().date())
	count = models.IntegerField(default= 0)	
