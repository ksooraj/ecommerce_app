from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponseRedirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
# from rest_framework.views import APIView
from django.contrib.auth import (
		REDIRECT_FIELD_NAME,
		login as auth_login,
		logout as auth_logout,
		authenticate)
from django.contrib.auth.mixins import LoginRequiredMixin,UserPassesTestMixin
# from django.contrib import messages
from django.views.generic import FormView, RedirectView,TemplateView
from django.http import HttpResponse
from .models import Category, Product, Subcategory, AddCategory_Counter, AddProduct_Counter
from datetime import datetime
import json
# Create your views here.


class Index_View(TemplateView):
	def get(self, request):
		print('checking')
		try:
			product_details = Product.objects.all()
			product_details_list = []
			subcategory_lsit = []
			for each in product_details:
				temp_dict = {}

				temp_dict['quantity'] = each.quantity
				temp_dict['price'] = each.price
				temp_dict['name'] = each.name
				temp_dict['category_name'] = each.category_name.category_name
				for each1 in Subcategory.objects.filter(category_name= each.category_name.category_name):
					subcategory_lsit.append(each1.name)
				temp_dict['subcategory_name'] = subcategory_lsit
				# print(temp_dict['subcategory_name'])
				product_details_list.append(temp_dict)

			return render(request, 'index.html', {'product_details': product_details_list})
		except Exception as e:
			print(e)
			return HttpResponseRedirect('/error')
	# template_name = "index.html"

class Login_View(FormView):
	# return render(request, 'login.html')
	# template_name = "login.html"
	def get(self, request):
		return render(request, 'login.html')
	def post(self,request):
		try:
			print('inside post method')
			username = self.request.POST['username']
			password = self.request.POST['password']
			user = authenticate(username=username, password=password)
			print("checking")
			if user is not None:
				if user.is_active:
					auth_login(request, user)
					return HttpResponseRedirect('/home')
				else:
					return HttpResponseRedirect('/login')
			else:
				return HttpResponseRedirect('/login')
		except Exception as e:
			print(e)
			return HttpResponseRedirect('/error')


class Home_View(APIView, LoginRequiredMixin,TemplateView):
	login_url='/login'
	
	def get(self, request):
		category = Category.objects.all()
		return render(request, 'home.html', {'category': category})
		# template_name = "home.html"

	def post(self,request):

		print('hellooo')
		try:
			flag = 0
			if not request.user.is_superuser:
				if AddProduct_Counter.objects.filter(user_name = request.user).exists():
					counter_details = AddProduct_Counter.objects.last()
					if counter_details.date_value == datetime.today().date():
						if counter_details.count < 9:
							counter_details.count += 1
							counter_details = AddProduct_Counter(user_name = request.user, count = counter_details.count, date_value = datetime.today().date()) 
							counter_details.save()
						else:
							flag = 1
							return Response({'message': 'Your daily limit is over'})
					# print(counter_details)
					else:
						AddProduct_Counter.objects.all().delete()
						counter_details = AddProduct_Counter(user_name = request.user, count = 0, date_value = datetime.today().date())
						counter_details.save()
				else:
					counter_details = AddProduct_Counter(user_name = request.user, count = 0, date_value = datetime.today().date())
					counter_details.save()
			if flag == 0:
				name = request.POST['name']
				category = request.POST['category']
				quantity = request.POST['quantity']
				price = request.POST['price']
				if Category.objects.filter(category_name = category).exists():
					pass
				else:
					category_details = Category(category_name= category)
					category_details.save()
				category = Category.objects.get(category_name= category) 
				print(category)
				# name = ''
				# for each in category:
				# 	name = each.category_name
				# print("hellooo")
				product_details = Product(name= name, price= int(price), quantity= int(quantity), category_name= category)
				product_details.save()


			# print(category)
			return Response({'message': 'Successfully Added'})
		except Exception as e:
			print(e)
			return response({'error': 'error occured'})

		

class Category_View(APIView,TemplateView):
	def get(self, request):
		# template_name = "category.html"
		# category = Category.objects.all()
		return render(request, 'category.html', {})
	
	def post(self,request):

		try:
			print(request.user.is_superuser)
			flag = 0
			if not request.user.is_superuser:
				print("not super user")
				if AddCategory_Counter.objects.filter(user_name = request.user).exists():
					counter_details = AddCategory_Counter.objects.last()
					# counter_details = AddCategory_Counter.objects.order_by('count')[0]
					print(counter_details)
					print(counter_details.count)
					if counter_details.date_value == datetime.today().date():
						if counter_details.count < 9:
							counter = counter_details.count 
							counter += 1
							print(counter)
							print(type(counter_details.count))
							print("helllooooo")
							counter_details = AddCategory_Counter(user_name = request.user, count = counter, date_value = datetime.today().date()) 
							counter_details.save()
						else:
							flag = 1
							return Response({'message': 'Your daily limit is over'})
					# print(counter_details)
					else:
						print("======next day==========")
						AddCategory_Counter.objects.all().delete()
						counter_details = AddCategory_Counter(user_name = request.user, count = 0, date_value = datetime.today().date())
						counter_details.save()
				else:
					counter_details = AddCategory_Counter(user_name = request.user, count = 0, date_value = datetime.today().date())
					counter_details.save()
			# print(todays_count)
			if flag == 0:
				category = request.POST['category']
				subcategory = request.POST['subcategory']
				if Category.objects.filter(category_name = category).exists():
					pass
				else:
					category_details = Category(category_name= category)
					category_details.save()
				category = Category.objects.get(category_name= category)
				subcategory_details = Subcategory(category_name= category, name= subcategory)
				subcategory_details.save()
			print(category)
			return Response({'message': 'Successfully Added'})
		except Exception as e:
			print("exception block")
			print(e)
			return response({'error': 'error occured'})


class CartView(APIView):
	def get(self, request):
		print('hiiiiiiiiiiii')
		product_list = json.loads(request.GET['product_details_list'])
		print(product_list)
		print('hellooo')
		return render(request, 'cart.html', {'product_list': product_list['product_details_list']})
	def post(slef, request):
		try:
			print("hellooooo")
			product_details_list = []
			product_list = json.loads(request.POST['product_list'])	
			print(type(product_list))
			for each in product_list:
				temp_dict = {}
				temp_list = []
				product = Product.objects.get(name= each)
				temp_dict['quantity'] = product.quantity
				temp_dict['price'] = product.price
				temp_dict['name'] = product.name
				temp_dict['category_name'] = product.category_name.category_name
				temp_list.append(temp_dict)

				print(product.name)

				product_details_list.append(temp_list)
				print(product)
			return Response({'product_details_list': product_details_list})
			
		except Exception as e:
			print(e)
			return response({'error': 'error occured'})



class Logout_view(APIView,TemplateView):
	def get(self, request):
		auth_logout(request)
		return HttpResponseRedirect('/')


class Error_View(APIView):
	# template_name = 'error.html'
	def get(self, request):
	# 	print('hellooohiii')
		# from django.contrib.auth import get_user_model
		# User = get_user_model()
		# user = User.objects.get(username="sooraj")
		# user.is_staff = True
		# user.is_admin = True
		# user.is_superuser = False
		# user.save()
		return render(request, 'error.html')
		# user.is_staff = True
		# user.is_admin = True
		# user.is_superuser = True
