from .views import *
from django.urls import path


urlpatterns = [
				path('', Index_View.as_view()),
				path('login', Login_View.as_view()),
				path('home', Home_View.as_view()),
				path('category', Category_View.as_view()),
				path('logout', Logout_view.as_view()),
				path('cart', CartView.as_view()),
				path('error', Error_View.as_view()),
				]